package managers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import entities.Score;
import services.ConnexionBd;


public class ScoreManager {

    public static String queryAll = "select * from scores";
    public static ArrayList<Score> getAllScores(Context ctx)
    {
        ArrayList<Score> listeScores = new ArrayList<>();
        SQLiteDatabase db = ConnexionBd.getBd(ctx);
        if (db.rawQuery(queryAll, null) == null)
        {
            return listeScores;
        }

        Cursor c = db.rawQuery(queryAll, null);

        while (c.moveToNext())
        {
            Score score = new Score(c.getInt(0), c.getString(1), c.getInt(2), c.getString(3));
            listeScores.add(score);
        }
        c.close();

        ConnexionBd.closeBD();

        return listeScores;
    }

    public static String getQueryByName = "select * from scores where nom_player=?";
    public static Score getScoreByName(Context ctx, String nom_player)
    {
        Score scoreReturn = null;
        SQLiteDatabase db = ConnexionBd.getBd(ctx);
        Cursor c = db.rawQuery(getQueryByName, new String[]{nom_player});

        while (c.moveToNext())
        {
            scoreReturn = new Score(c.getInt(0), c.getString(1), c.getInt(2), c.getString(3));
        }
        c.close();

        ConnexionBd.closeBD();

        return scoreReturn;
    }


    public static int ajouterScore(Context ctx, Score score)
    {
        int index = 0;

        SQLiteDatabase db = ConnexionBd.getBd(ctx);

        ContentValues cv = new ContentValues();
        cv.put("nom_player", score.getNamePlayer());
        cv.put("parties_gagnees", score.getScorePlayer());
        cv.put("image_player", score.getImagePlayer());

        db.insert("scores", "", cv);

        return index;
    }

    public static int updateScore(Context ctx, Score score)
    {
        int index = 0;

        SQLiteDatabase db = ConnexionBd.getBd(ctx);

        ContentValues cv = new ContentValues();
        cv.put("nom_player", score.getNamePlayer());
        cv.put("parties_gagnees", score.getScorePlayer() + 1);
        cv.put("image_player", score.getImagePlayer());

        db.update("scores", cv, "id_score=?", new String[]{String.valueOf(score.getIdPlayer())});

        return index;
    }

}
