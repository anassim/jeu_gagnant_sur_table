package managers;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.tp_android.R;
import com.example.tp_android.ScoreActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import entities.Score;


public class ScoreAdaptater extends ArrayAdapter {

    int idLayout;
    Context ctx;
    public ScoreAdaptater(@NonNull Context context, int resource, @NonNull List<Score> objects) {
        super(context, resource, objects);

        this.idLayout = resource;
        this.ctx = context;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final Score score = (Score)getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(ctx).inflate(this.idLayout, null);
        }

        ImageView img = convertView.findViewById(R.id.image_score);
        try {
            InputStream in = ctx.openFileInput(score.getImagePlayer());
            byte[] bArr = new byte[ctx.openFileInput(score.getImagePlayer()).available()];
            in.read(bArr);
            in.close();

            img.setImageBitmap(BitmapFactory.decodeByteArray(bArr,0,bArr.length));
        } catch (IOException e) {
            e.printStackTrace();
        }



        TextView txtName = convertView.findViewById(R.id.nom_player);
        txtName.setText(score.getNamePlayer());
        TextView txtScore = convertView.findViewById(R.id.score_player);
        txtScore.setText(""+score.getScorePlayer());

        return convertView;
    }
}
