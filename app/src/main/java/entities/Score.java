package entities;

public class Score {

    private int idPlayer;
    private String namePlayer;
    private int scorePlayer;
    private String imagePlayer;

    public Score(int idPlayer, String namePlayer, int scorePlayer, String imagePlayer) {
        this.idPlayer = idPlayer;
        this.namePlayer = namePlayer;
        this.scorePlayer = scorePlayer;
        this.imagePlayer = imagePlayer;
    }
    public Score(String namePlayer, int scorePlayer, String imagePlayer) {
        this.namePlayer = namePlayer;
        this.scorePlayer = scorePlayer;
        this.imagePlayer = imagePlayer;
    }
    public int getIdPlayer() {
        return idPlayer;
    }
    public void setIdPlayer(int idPlayer) {
        this.idPlayer = idPlayer;
    }
    public String getNamePlayer() {
        return namePlayer;
    }
    public void setNamePlayer(String namePlayer) {
        this.namePlayer = namePlayer;
    }
    public int getScorePlayer() {
        return scorePlayer;
    }
    public void setScorePlayer(int scorePlayer) {
        this.scorePlayer = scorePlayer;
    }
    public String getImagePlayer() {
        return imagePlayer;
    }
    public void setImagePlayer(String imagePlayer) {
        this.imagePlayer = imagePlayer;
    }
}
