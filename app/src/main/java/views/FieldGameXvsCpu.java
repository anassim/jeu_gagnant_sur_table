package views;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.view.ViewGroup;


public class FieldGameXvsCpu extends ViewGroup {

    Handler handler;
    StrickerAI strickerAI;
    StrickerPlayer1 strickerPlayer1;
    Puck puck;
    public FieldGameXvsCpu(Context context, Handler handler) {
        super(context);
        this.handler = handler;

        this.strickerAI = new StrickerAI(context, this.handler, this);
        this.strickerPlayer1 = new StrickerPlayer1(context,  this.handler, this);

        //this.puck = new Puck(context,  this.handler, new);

        this.addView(this.strickerAI);
        this.addView(this.strickerPlayer1);
        this.addView(this.puck);
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
    //--------------------------------------------------------------------------------------------------------------------------------
}