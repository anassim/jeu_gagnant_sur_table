package views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;

import com.example.tp_android.R;


public class StrickerPlayer1 extends View implements Runnable {

    Context ctx;
    Handler handler;
    FieldGameXvsY fieldY;
    FieldGameXvsCpu fieldCpu;
    float xPlayer1, yPlayer1;
    float rayonPlayer1;
    float vStrickInitial;
    float vx;
    float vy;
    float distanceAvecJeton;
    float massePlayer1;
    float cos;
    float sin;
    Paint p1;
    Bitmap imageStrickerPlayer1;
    RectF rectPlayer1;
    public StrickerPlayer1(Context context, Handler handler, FieldGameXvsY field) {
        super(context);
        this.ctx = context;
        this.handler = handler;
        this.fieldY = field;
        this.rayonPlayer1 = 75;
        this.vStrickInitial = 0;
        this.vx = 0;
        this.vy = 0;
        this.cos = 0;
        this.sin = 0;
        this.distanceAvecJeton = 0;
        this.massePlayer1 = 60; //c en gramme
        this.p1 = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.p1.setTextSize(45);
        this.imageStrickerPlayer1 = BitmapFactory.decodeResource(getResources(), R.drawable.stricker_player_1);
        this.rectPlayer1 = new RectF(0, 0, this.rayonPlayer1 * 2, this.rayonPlayer1 * 2);

    }
    public StrickerPlayer1(Context context, Handler handler, FieldGameXvsCpu field) {
        super(context);
        this.ctx = context;
        this.handler = handler;
        this.fieldCpu = fieldCpu;
        this.rayonPlayer1 = 75;
        this.p1 = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.p1.setTextSize(20);
        this.imageStrickerPlayer1 = BitmapFactory.decodeResource(getResources(), R.drawable.stricker_player_1);
        this.rectPlayer1 = new RectF(0, 0, this.rayonPlayer1 * 2, this.rayonPlayer1 * 2);
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        this.xPlayer1 = left + this.rayonPlayer1;
        this.yPlayer1 = top + this.rayonPlayer1;
    }
    @Override
    protected void onDraw(Canvas canvas) {
        //on dessine notre palet sur le canvas
        canvas.drawBitmap(this.imageStrickerPlayer1, null, this.rectPlayer1, this.p1);
        invalidate();
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    //deplacement en mouvement vitesse
    public boolean deplacerPlayer1(float x2, float y2, float vx, float vy) {
        //si le event est dans le cote du player 1 on deplace sa palet
        if (y2 > ((this.fieldY.getHeight() / 2) + rayonPlayer1)) {
            //deplacer la palet
            this.layout((int) (x2 - rayonPlayer1), (int) (y2 - rayonPlayer1), (int) (x2 + rayonPlayer1), (int) (y2 + rayonPlayer1));
            //enregistrer la vitesse de la palet
            this.vx = vx;
            this.vy = vy;
            //verefier la collision avec le jeton
            if (Math.sqrt(Math.pow((x2 - this.fieldY.puck.xPuck), 2) + Math.pow((y2 - (int) this.fieldY.puck.yPuck), 2)) <= (this.rayonPlayer1 + this.fieldY.puck.rayonPuck)) {
                /**calculer la collision selon les forces.
                 *  et les vitesse
                 */
                //en cas de collision il faut faire sortir le jeton de la palet
                    this.fieldY.puck.sortirDeLaPalet(this.xPlayer1, this.yPlayer1, this.rayonPlayer1, vx, vy);

                //collision avec le jeton
                lancerJeton();

                //relancer le thread du jeton
                this.handler.removeCallbacks(this.fieldY.puck);
                this.handler.post(this.fieldY.puck);
            }
            return true;
        }
        //si on a a bouge la palet on return le true sinon le false
        return false;
    }

    //--------------------------------------------------------------------------------------------------------------------------------

    public void lancerJeton()
    {
        this.vStrickInitial = (float)Math.sqrt((this.vx * this.vx) + (this.vy * this.vy));
        //this.fieldY.puck.vPuckFinal = (float)Math.sqrt((this.fieldY.puck.vxPuck * this.fieldY.puck.vxPuck) + (this.fieldY.puck.vyPuck * this.fieldY.puck.vyPuck))
        this.fieldY.puck.vPuckFinal = this.vStrickInitial - this.fieldY.puck.vPuckFinal;
        this.distanceAvecJeton = (float)Math.sqrt(Math.pow(this.xPlayer1 - this.fieldY.puck.xPuck,2)+Math.pow(this.yPlayer1 - this.fieldY.puck.yPuck, 2));
        this.cos = (this.fieldY.puck.xPuck - this.xPlayer1) / this.distanceAvecJeton;
        this.sin = (this.fieldY.puck.yPuck - this.yPlayer1) / this.distanceAvecJeton;

        this.fieldY.puck.vxPuck = this.cos * this.fieldY.puck.vPuckFinal;
        this.fieldY.puck.vyPuck = this.sin * this.fieldY.puck.vPuckFinal;
    }

    //--------------------------------------------------------------------------------------------------------------------------------
    @Override
    public void run() {
    }
}
