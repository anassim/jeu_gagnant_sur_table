package views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.View;

import com.example.tp_android.R;


public class StrickerPlayer2 extends View implements Runnable {

    Context ctx;
    Handler handler;
    FieldGameXvsY field;
    float xPlayer2, yPlayer2;
    float rayonPlayer2;
    float vStrickInitial;
    float vx;
    float vy;
    float distanceAvecJeton;
    float cos;
    float sin;
    float massePlayer2;
    Paint p2;
    Bitmap imageStrickerPlayer2;
    RectF rectPlayer2;
    public StrickerPlayer2(Context context, Handler handler, FieldGameXvsY field) {
        super(context);
        this.ctx = context;
        this.handler = handler;
        this.field = field;
        this.rayonPlayer2 = 75;
        this.vStrickInitial = 0;
        this.vx = 0;
        this.vy = 0;
        this.cos = 0;
        this.sin = 0;
        this.distanceAvecJeton = 0;
        this.massePlayer2 = 60;
        this.p2 = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.p2.setTextSize(45);
        this.imageStrickerPlayer2 = BitmapFactory.decodeResource(getResources(), R.drawable.stricker_player_2);
        this.rectPlayer2 = new RectF(0, 0, this.rayonPlayer2 * 2, this.rayonPlayer2 * 2);
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        this.xPlayer2 = left + this.rayonPlayer2;
        this.yPlayer2 = top + this.rayonPlayer2;
    }
    @Override
    protected void onDraw(Canvas canvas) {
        //on dessine notre palet sur le canvas
        canvas.drawBitmap(this.imageStrickerPlayer2, null, this.rectPlayer2, this.p2);
        invalidate();
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    //deplacement en mouvement avec vitesse
    public boolean deplacerPlayer2(float x2, float y2, float vx, float vy) {
        //si le event est dans le cote du player 2 on deplace sa palet
        if (y2 < ((this.field.getHeight() / 2) - rayonPlayer2)) {
            //deplacer la palet
            this.layout((int) (x2 - rayonPlayer2), (int) (y2 - rayonPlayer2), (int) (x2 + rayonPlayer2), (int) (y2 + rayonPlayer2));
            //enregistrer la vitesse de la palet
            this.vx = vx;
            this.vy = vy;
            //verefier la collision avec le jeton
            if (Math.sqrt(Math.pow((x2 - this.field.puck.xPuck), 2) + Math.pow((y2 - this.field.puck.yPuck), 2)) <= (this.rayonPlayer2 + this.field.puck.rayonPuck)) {
                /**calculer la collision selon les forces
                 * et les masses et les vitesses
                 */
                //en cas de collision il faut faire sortir le jeton de la palet
                this.field.puck.sortirDeLaPalet(this.xPlayer2, this.yPlayer2, this.rayonPlayer2, vx, vy);

                if (vx != 0 || vy != 0) {
                    this.field.puck.vxPuck = (this.massePlayer2/this.field.puck.massePuck * vx) + this.field.puck.vxPuck;
                    this.field.puck.vyPuck = (this.massePlayer2/this.field.puck.massePuck * vy) + this.field.puck.vyPuck;
                }
                //relancer le jeton dans la direction
                this.handler.removeCallbacks(this.field.puck);
                this.handler.post(this.field.puck);
            }
            return true;
        }
        //si on a a bouge la palet on return le true sinon le false
        return false;
    }

    //--------------------------------------------------------------------------------------------------------------------------------

    public void lancerJeton()
    {
        this.vStrickInitial = (float)Math.sqrt((this.vx * this.vx) + (this.vy * this.vy));
        //this.fieldY.puck.vPuckFinal = (float)Math.sqrt((this.fieldY.puck.vxPuck * this.fieldY.puck.vxPuck) + (this.fieldY.puck.vyPuck * this.fieldY.puck.vyPuck))
        this.field.puck.vPuckFinal = this.vStrickInitial - this.field.puck.vPuckFinal;
        this.distanceAvecJeton = (float)Math.sqrt(Math.pow(this.xPlayer2 - this.field.puck.xPuck,2)+Math.pow(this.yPlayer2 - this.field.puck.yPuck, 2));
        this.cos = (this.field.puck.xPuck - this.xPlayer2) / this.distanceAvecJeton;
        this.sin = (this.field.puck.yPuck - this.yPlayer2) / this.distanceAvecJeton;

        this.field.puck.vxPuck = this.cos * this.field.puck.vPuckFinal;
        this.field.puck.vyPuck = this.sin * this.field.puck.vPuckFinal;
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    @Override
    public void run() {
    }
}