package views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tp_android.PartieXvsYActivity;
import com.example.tp_android.R;
import com.example.tp_android.XvsYActivity;

import java.io.IOException;
import java.io.InputStream;

import entities.Score;
import managers.ScoreManager;


public class FieldGameXvsY extends ViewGroup implements GestureDetector.OnGestureListener {

    Context ctx;
    Handler handler;
    PartieXvsYActivity partie;
    XvsYActivity menu;
    public static MediaPlayer good_game_sound;
    public static MediaPlayer score_sound;
    boolean partieFinished;
    public Score scoreWinner;
    public ImageView imageView;
    GestureDetector gd;
    VelocityTracker vt;
    StrickerPlayer1 strickerPlayer1;
    StrickerPlayer2 strickerPlayer2;
    String namePlayer1;
    String namePlayer2;
    int scorePlayer1;
    int scorePlayer2;
    Puck puck;
    RectF butRect1;
    RectF butRect2;
    public static int nbSecondeVitesse = 100;
    public FieldGameXvsY(Context context, Handler handler, PartieXvsYActivity partie, XvsYActivity menu) {
        super(context);
        this.ctx = context;
        this.handler = handler;
        this.partie = partie;
        this.menu = menu;
        this.partieFinished = false;
        this.gd = new GestureDetector(this.ctx, this);
        this.strickerPlayer1 = new StrickerPlayer1(context, this.handler, this);
        this.strickerPlayer2 = new StrickerPlayer2(context, this.handler, this);
        //this.namePlayer1 = ((PartieXvsYActivity)getParent()).getIntent().getStringExtra("name1");
        //this.namePlayer2 = ((PartieXvsYActivity)getParent()).getIntent().getStringExtra("name2");
        this.namePlayer1 = this.partie.name1;
        this.namePlayer2 = this.partie.name2;
        this.scorePlayer1 = 0;
        this.scorePlayer2 = 0;
        this.puck = new Puck(context, this.handler, this);
        this.addView(this.strickerPlayer1);
        this.addView(this.strickerPlayer2);
        this.addView(this.puck);
        this.setWillNotDraw(false);
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        //positionner les trois palet au debut du jeu
        int r1 = (int) strickerPlayer1.rayonPlayer1;
        int r2 = (int) strickerPlayer2.rayonPlayer2;
        int rp = (int) puck.rayonPuck;
        this.strickerPlayer1.layout(getWidth() / 2 - r1, getHeight() - 100 - r1, getWidth() / 2 + r1, getHeight() - 100 + r1);
        this.strickerPlayer2.layout(getWidth() / 2 - r2, 100 - r2, getWidth() / 2 + r2, 100 + r2);
        //this.puck.layout(getWidth() / 2 - rp, getHeight() / 2 - rp, getWidth() / 2 + rp, getHeight() / 2 + rp);
        this.puck.layout(getWidth() / 2 - rp, getHeight()/2 - rp, getWidth() / 2 + rp, getHeight()/2 + rp);

        this.butRect1 = new RectF(2*getWidth() / 7, - (getHeight() / 7), 5*getWidth()/7, getHeight() / 7);
        this.butRect2 = new RectF(2*getWidth() / 7, getHeight() - getHeight()/7, 5*getWidth()/7, getHeight() + getHeight()/7);
    }
    @Override
    protected void onDraw(Canvas canvas) {
        this.strickerPlayer1.p1.setColor(Color.BLACK);

        //dessiner les deux barres des joueurs seulon l'ecran
        this.strickerPlayer1.p1.setTextSize(45);
        this.strickerPlayer1.p1.setStrokeWidth(3);
        this.strickerPlayer1.p1.setStyle(Paint.Style.STROKE);
        this.strickerPlayer2.p2.setStrokeWidth(3);
        this.strickerPlayer2.p2.setStyle(Paint.Style.STROKE);
        canvas.drawArc(this.butRect1, 0F, 180F, true, this.strickerPlayer1.p1);
        canvas.drawArc(this.butRect2, 180F, 180F , true, this.strickerPlayer2.p2);
        canvas.drawLine(0, getHeight() / 2, getWidth(), getHeight()/2, strickerPlayer1.p1);
        canvas.drawCircle(getWidth()/2, getHeight()/2, 2*getWidth() / 8, strickerPlayer2.p2);

        this.strickerPlayer1.p1.setStyle(Paint.Style.FILL);
        this.strickerPlayer2.p2.setStyle(Paint.Style.FILL);
        canvas.drawCircle(getWidth()/2, getHeight()/2,20, strickerPlayer1.p1);

        this.strickerPlayer1.p1.setStyle(Paint.Style.STROKE);
        this.strickerPlayer2.p2.setStyle(Paint.Style.STROKE);
        this.strickerPlayer1.p1.setStrokeWidth(2);
        this.strickerPlayer2.p2.setStrokeWidth(2);
        canvas.drawText(""+scorePlayer1, getWidth() - 50,getHeight()/2 - 100,strickerPlayer1.p1);
        canvas.drawText(""+scorePlayer2, getWidth() - 50,getHeight()/2 + 100, strickerPlayer2.p2);

//        if (this.partieFinished)
//        {
//            this.strickerPlayer1.p1.setStyle(Paint.Style.FILL);
//            this.strickerPlayer1.p1.setStrokeWidth(10);
//            this.strickerPlayer1.p1.setTextSize(150);
//            this.strickerPlayer1.p1.setColor(Color.parseColor("#DAA520"));
//            canvas.drawText("Winner", getWidth()/2 - 235,getHeight()/2 - 100,strickerPlayer1.p1);

//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        }

    }
    //--------------------------------------------------------------------------------------------------------------------------------

    protected void afficherWinner(final String nameWinner)
    {
        this.partieFinished = true;
        invalidate();

        good_game_sound = MediaPlayer.create(this.partie, R.raw.good_game);
        good_game_sound.start();

        scoreWinner = ScoreManager.getScoreByName(this.ctx, nameWinner);
        /**
         * remplir le score avec les informations du gagnant
         */
        if (scoreWinner == null)
        {
            scoreWinner = new Score(nameWinner, 1, nameWinner+".png");
            ScoreManager.ajouterScore(FieldGameXvsY.this.ctx, scoreWinner);
        }
        else if (scoreWinner != null)
        {
            ScoreManager.updateScore(FieldGameXvsY.this.ctx, scoreWinner);
        }

        AlertDialog.Builder builderWin = new AlertDialog.Builder(getContext());
        builderWin.setView(this.partie.getLayoutInflater().inflate(R.layout.layout_winner, null));

        AlertDialog fenetreWin = builderWin.create();
        fenetreWin.show();

        /**
         *  remplir les champs du dialog avec les informations du score
         */
            imageView = fenetreWin.findViewById(R.id.image_winner);
        try {
            InputStream in = ctx.openFileInput(scoreWinner.getImagePlayer());
            byte[] bArr = new byte[ctx.openFileInput(scoreWinner.getImagePlayer()).available()];
            in.read(bArr);
            in.close();

            imageView.setImageBitmap(BitmapFactory.decodeByteArray(bArr,0,bArr.length));
        } catch (IOException e) {
            e.printStackTrace();
        }


        TextView nameView = fenetreWin.findViewById(R.id.nom_winner);
        nameView.setText(nameWinner);

        TextView scoreView = fenetreWin.findViewById(R.id.score_winner);
        scoreView.setText(scorePlayer1+" - "+scorePlayer2);

        TextView rejouerView = fenetreWin.findViewById(R.id.rejouer_winner);
        rejouerView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FieldGameXvsY.this.menu, PartieXvsYActivity.class);
                intent.putExtra("name1",namePlayer1);
                intent.putExtra("name2",namePlayer2);
                FieldGameXvsY.this.menu.startActivityForResult(intent, 1);
                FieldGameXvsY.this.partie.finish();
            }
        });

        TextView takePhotoView = fenetreWin.findViewById(R.id.take_photo_winner);
        takePhotoView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (intent.resolveActivity(FieldGameXvsY.this.partie.getPackageManager()) != null) {
                    FieldGameXvsY.this.partie.startActivityForResult(intent, 3);
                }
            }
        });

        TextView menuView = fenetreWin.findViewById(R.id.menu_winner);
        menuView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FieldGameXvsY.this.partie.finish();
            }
        });
    }

    //--------------------------------------------------------------------------------------------------------------------------------

    public void playerScored()
    {
        score_sound = MediaPlayer.create(this.partie, R.raw.scored);
        score_sound.setVolume(20, 20);
        score_sound.start();

        //la partie finis en arrivant a 7
        if (scorePlayer1 == 3)
        {
            //affichage du gagnant
            afficherWinner(this.namePlayer1);
        }
        else if (scorePlayer2 == 3)
        {
            afficherWinner(this.namePlayer2);
        }

        this.strickerPlayer1.vStrickInitial = 0;
        this.strickerPlayer2.vStrickInitial = 0;
        this.strickerPlayer1.vx = 0;
        this.strickerPlayer2.vx = 0;
        this.strickerPlayer1.vy = 0;
        this.strickerPlayer2.vy = 0;
        this.puck.vPuckFinal = 0;
        this.puck.vxPuck = 0;
        this.puck.vyPuck = 0;

        //positionner les trois palet au debut du jeu
        int r1 = (int) strickerPlayer1.rayonPlayer1;
        int r2 = (int) strickerPlayer2.rayonPlayer2;
        int rp = (int) puck.rayonPuck;
        this.strickerPlayer1.layout(getWidth() / 2 - r1, getHeight() - 100 - r1, getWidth() / 2 + r1, getHeight() - 100 + r1);
        this.strickerPlayer2.layout(getWidth() / 2 - r2, 100 - r2, getWidth() / 2 + r2, 100 + r2);
        this.puck.layout(getWidth() / 2 - rp, getHeight()/2 - rp, getWidth() / 2 + rp, getHeight()/2 + rp);

        invalidate();

    }

    //--------------------------------------------------------------------------------------------------------------------------------
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_UP) {
            if (event.getY(event.getActionIndex()) > ((this.getHeight() / 2) + strickerPlayer1.rayonPlayer1)) {
                strickerPlayer1.vx = 0;
                strickerPlayer1.vy = 0;
                strickerPlayer1.vStrickInitial = 0;
            } else if (event.getY(event.getActionIndex()) < ((this.getHeight() / 2) - strickerPlayer2.rayonPlayer2)) {
                strickerPlayer2.vx = 0;
                strickerPlayer2.vy = 0;
                strickerPlayer2.vStrickInitial = 0;
            }
        }
        this.gd.onTouchEvent(event);
        return true;
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    @Override
    public boolean onDown(MotionEvent e) {
        if (this.vt == null) {
            this.vt = VelocityTracker.obtain();
        } else {
            this.vt.clear();
        }
        this.vt.addMovement(e);
        this.vt.computeCurrentVelocity(nbSecondeVitesse);
        if (Math.sqrt(Math.pow((e.getX(e.getActionIndex()) - this.puck.xPuck), 2) + Math.pow((e.getY(e.getActionIndex()) - (int) this.puck.yPuck), 2)) > (this.strickerPlayer1.rayonPlayer1 + this.puck.rayonPuck)) {
            for (int i = 0; i < e.getPointerCount(); i++) {
                //soit on deplace le player 1
                if (this.strickerPlayer1.deplacerPlayer1(e.getX(i), e.getY(i), this.vt.getXVelocity(i), this.vt.getYVelocity(i))) {
                    //memoriser les dernieres coordonnees de la palet
                    this.strickerPlayer1.xPlayer1 = e.getX(i);
                    this.strickerPlayer1.yPlayer1 = e.getY(i);
                }
                //soit on deplace le player 2
                else if (this.strickerPlayer2.deplacerPlayer2(e.getX(i), e.getY(i), this.vt.getXVelocity(i), this.vt.getYVelocity(i))) {
                    //memoriser les dernieres coordonnees de la palet
                    this.strickerPlayer2.xPlayer2 = e.getX(i);
                    this.strickerPlayer2.yPlayer2 = e.getY(i);
                }
            }
        }
        return true;
    }
    @Override
    public void onShowPress(MotionEvent e) {
    }
    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        if (this.vt == null) {
            this.vt = VelocityTracker.obtain();
        } else {
            this.vt.clear();
        }
        this.vt.addMovement(e2);
        this.vt.computeCurrentVelocity(nbSecondeVitesse);
        //si notre doigt n'est pas deja sur le jeton donc on peut bouger le palet
        if (Math.sqrt(Math.pow((e1.getX(e2.getActionIndex()) - this.puck.xPuck), 2) + Math.pow((e1.getY(e2.getActionIndex()) - (int) this.puck.yPuck), 2)) > (this.strickerPlayer1.rayonPlayer1 + this.puck.rayonPuck)) {
            for (int i = 0; i < e2.getPointerCount(); i++) {
                //soit on deplace le player 1
                if (this.strickerPlayer1.deplacerPlayer1(e2.getX(i), e2.getY(i), this.vt.getXVelocity(i), this.vt.getYVelocity(i))) {
                    //memoriser les dernieres coordonnees de la palet
                    this.strickerPlayer1.xPlayer1 = e2.getX(i);
                    this.strickerPlayer1.yPlayer1 = e2.getY(i);
                }
                //soit on deplace le player 2
                else if (this.strickerPlayer2.deplacerPlayer2(e2.getX(i), e2.getY(i), this.vt.getXVelocity(i), this.vt.getYVelocity(i))) {
                    //memoriser les dernieres coordonnees de la palet
                    this.strickerPlayer2.xPlayer2 = e2.getX(i);
                    this.strickerPlayer2.yPlayer2 = e2.getY(i);
                }
            }
        }
        return true;
    }
    @Override
    public void onLongPress(MotionEvent e) {
    }
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return true;
    }
}
