package views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;

import com.example.tp_android.R;

import android.os.Handler;


public class StrickerAI extends View implements Runnable {

    Context ctx;
    Handler handler;
    FieldGameXvsCpu field;
    float xAI, yAI;
    float rayonAI;
    Paint pAI;
    Bitmap imageStrickerAI;
    RectF rectAI;

    public StrickerAI(Context context, Handler handler, FieldGameXvsCpu field) {
        super(context);
        this.ctx = context;
        this.handler = handler;
        this.field = field;
        this.rayonAI = 50;
        this.pAI = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.pAI.setTextSize(20);
        this.imageStrickerAI = BitmapFactory.decodeResource(getResources(), R.drawable.stricker_player_2);
        this.rectAI = new RectF(xAI, yAI, xAI + this.rayonAI * 2, yAI + this.rayonAI * 2);

    }
    //--------------------------------------------------------------------------------------------------------------------------------
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    public void deplacerAI(float x, float y) {
    }
    //--------------------------------------------------------------------------------------------------------------------------------

    @Override
    public void run() {
    }
}
