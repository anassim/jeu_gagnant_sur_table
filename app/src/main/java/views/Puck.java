package views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.view.View;

import com.example.tp_android.R;

import android.os.Handler;


public class Puck extends View implements Runnable {

    Context ctx;
    Handler handler;
    FieldGameXvsY fieldXvsY;
    float xPuck, yPuck;
    int xstep, ystep;
    float rayonPuck;
    float vPuckFinal;
    float cos,sin;
    float vxPuck;
    float vyPuck;
    float vSwap;
    float r1, r2;
    float massePuck;
    int tempsV;
    boolean isRunning = false;
    boolean turnJeton = false;
    Paint pp;
    Bitmap imagePuck;
    RectF rectPuck;
    public Puck(Context context, Handler handler, FieldGameXvsY field) {
        super(context);
        this.ctx = context;
        this.handler = handler;
        this.fieldXvsY = field;
        this.xstep = +1;
        this.ystep = +1;
        this.rayonPuck = 40;
        this.vPuckFinal = 0;
        this.cos = 0;
        this.sin = 0;
        this.vxPuck = 0;
        this.vyPuck = 0;
        this.vSwap = 0;
        this.massePuck = 40;// en gramme
        this.tempsV = 15;
        this.pp = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.imagePuck = BitmapFactory.decodeResource(getResources(), R.drawable.jeton);
        this.rectPuck = new RectF(0, 0, rayonPuck * 2, rayonPuck * 2);
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        this.xPuck = left + this.rayonPuck;
        this.yPuck = top + this.rayonPuck;
    }
    @Override
    protected void onDraw(Canvas canvas) {
        //on dessine notre palet sur le canvas
        canvas.drawBitmap(this.imagePuck, null, this.rectPuck, this.pp);
        invalidate();
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    public void deplacerPuckByVelocity() {
            this.xPuck += this.vxPuck * this.tempsV / FieldGameXvsY.nbSecondeVitesse;
            this.yPuck += this.vyPuck * this.tempsV / FieldGameXvsY.nbSecondeVitesse;
            this.vPuckFinal = (float) Math.sqrt((this.vxPuck * this.vxPuck) + (this.vyPuck * this.vyPuck));
            this.isRunning = true;

        //deplacer le jeton avec les x et y calcules
        this.layout((int) (this.xPuck - rayonPuck), (int) (this.yPuck - rayonPuck), (int) (this.xPuck + rayonPuck), (int) (this.yPuck + rayonPuck));

        if ((this.xPuck>2*this.fieldXvsY.getWidth() / 7 && this.xPuck<5*this.fieldXvsY.getWidth() / 7) && (this.yPuck <= 0))
        {
            this.fieldXvsY.scorePlayer1 += 1;
            this.fieldXvsY.playerScored();
            return;
        }
        else if ((this.xPuck>2*this.fieldXvsY.getWidth() / 7 && this.xPuck<5*this.fieldXvsY.getWidth() / 7) && (this.yPuck >= this.fieldXvsY.getHeight()))
        {
            this.fieldXvsY.scorePlayer2 += 1;
            this.fieldXvsY.playerScored();
            return;
        }

        // verefier si on a touche un mur
        if (this.xPuck <= this.rayonPuck) {
            this.vxPuck *= (-1);
            this.xPuck = this.rayonPuck;
            this.turnJeton = true;
        } else if (this.xPuck >= (this.fieldXvsY.getWidth() - this.rayonPuck)) {
            this.vxPuck *= (-1);
            this.xPuck = this.fieldXvsY.getWidth() - this.rayonPuck;
            this.turnJeton = true;
        } else if (this.yPuck <= 0) {
            this.vyPuck *= (-1);
            this.yPuck = this.rayonPuck;
            this.turnJeton = true;
        } else if (this.yPuck >= (this.fieldXvsY.getHeight())) {
            this.vyPuck *= (-1);
            this.yPuck = this.fieldXvsY.getHeight() - this.rayonPuck;
            this.turnJeton = true;
        }
        //si ya collision entre le jeton et une des palettes donc on calcule tout
        if (Math.sqrt(Math.pow((this.xPuck - this.fieldXvsY.strickerPlayer1.xPlayer1), 2) + Math.pow((this.yPuck - this.fieldXvsY.strickerPlayer1.yPlayer1), 2)) <= (this.rayonPuck + this.fieldXvsY.strickerPlayer1.rayonPlayer1)) {
            this.resortirLeJeton();

            //calculer les nouveaux vx et vy pour les deminuer en cas de collision contraire
            this.fieldXvsY.strickerPlayer1.vStrickInitial = (float)Math.sqrt((this.fieldXvsY.strickerPlayer1.vx * this.fieldXvsY.strickerPlayer1.vx) + (this.fieldXvsY.strickerPlayer1.vy * this.fieldXvsY.strickerPlayer1.vy));
            this.cos = this.vxPuck / this.vPuckFinal;
            this.sin = this.vyPuck / this.vPuckFinal;
            this.vPuckFinal = Math.abs(this.fieldXvsY.strickerPlayer1.vStrickInitial - this.vPuckFinal);
            this.vxPuck = this.cos * this.vPuckFinal;
            this.vyPuck = this.sin * this.vPuckFinal;

            //si une des vitesse x ou y est 0
            if (Math.abs(this.vyPuck * 100) < 1 || Math.abs(this.vxPuck * 100) < 1)
            {
                // si le jeton ne touche pas 1 des 4 cote du palet
                if ((this.xPuck - this.fieldXvsY.strickerPlayer1.xPlayer1) == 0 || (this.yPuck - this.fieldXvsY.strickerPlayer1.yPlayer1) == 0)
                {
                    this.vxPuck = (-1) * this.vxPuck;
                    this.vyPuck = (-1) * this.vyPuck;
                }
                //si il touche 1 des 4 cote du palet
                else
                {
                        if (((this.xPuck - this.fieldXvsY.strickerPlayer1.xPlayer1) > 0 && (this.yPuck - this.fieldXvsY.strickerPlayer1.yPlayer1) < 0) || ((this.xPuck - this.fieldXvsY.strickerPlayer1.xPlayer1) < 0 && (this.yPuck - this.fieldXvsY.strickerPlayer1.yPlayer1) > 0) )
                        {
                            this.vSwap = this.vxPuck;
                            this.vxPuck = this.vyPuck;
                            this.vyPuck = this.vSwap;
                        }
                        else
                        {
                            this.vSwap = (-1) * this.vxPuck;
                            this.vxPuck = (-1) * this.vyPuck;
                            this.vyPuck = this.vSwap;
                        }
                }
            }
            //si le vercteur est en diagonal
            else
            {
                if (Math.abs(this.yPuck - this.fieldXvsY.strickerPlayer1.yPlayer1) / Math.abs(this.xPuck - this.fieldXvsY.strickerPlayer1.xPlayer1) < 1)
                {
                    resortirLeJeton();
                    this.vxPuck = (-1) * this.vxPuck;
                }
                else if (Math.abs(this.yPuck - this.fieldXvsY.strickerPlayer1.yPlayer1) / Math.abs(this.xPuck - this.fieldXvsY.strickerPlayer1.xPlayer1) > 1)
                {
                    this.vyPuck = (-1) * this.vyPuck;
                }
                else
                {
                    this.vxPuck = (-1) * this.vxPuck;
                    this.vyPuck = (-1) * this.vyPuck;
                }
            }
            this.turnJeton = true;

        } else if (Math.sqrt(Math.pow((this.xPuck - this.fieldXvsY.strickerPlayer2.xPlayer2), 2) + Math.pow((this.yPuck - this.fieldXvsY.strickerPlayer2.yPlayer2), 2)) <= (this.rayonPuck + this.fieldXvsY.strickerPlayer2.rayonPlayer2)) {
            resortirLeJeton();

            //calculer les nouveaux vx et vy pour les deminuer en cas de collision contraire
            this.fieldXvsY.strickerPlayer2.vStrickInitial = (float)Math.sqrt((this.fieldXvsY.strickerPlayer2.vx * this.fieldXvsY.strickerPlayer2.vx) + (this.fieldXvsY.strickerPlayer2.vy * this.fieldXvsY.strickerPlayer2.vy));
            this.cos = this.vxPuck / this.vPuckFinal;
            this.sin = this.vyPuck / this.vPuckFinal;
            this.vPuckFinal = Math.abs(this.fieldXvsY.strickerPlayer2.vStrickInitial - this.vPuckFinal);
            this.vxPuck = this.cos * this.vPuckFinal;
            this.vyPuck = this.sin * this.vPuckFinal;

            //si une des vitesse x ou y est 0
            if (Math.abs(this.vyPuck * 100) < 1 || Math.abs(this.vxPuck * 100) < 1)
            {
                // si le jeton ne touche pas 1 des 4 cote du palet
                if ((this.xPuck - this.fieldXvsY.strickerPlayer2.xPlayer2) == 0 || (this.yPuck - this.fieldXvsY.strickerPlayer2.yPlayer2) == 0)
                {
                    this.vxPuck = (-1) * this.vxPuck;
                    this.vyPuck = (-1) * this.vyPuck;
                }
                //si il touche 1 des 4 cote du palet
                else
                {
                    if (((this.xPuck - this.fieldXvsY.strickerPlayer2.xPlayer2) > 0 && (this.yPuck - this.fieldXvsY.strickerPlayer2.yPlayer2) < 0) || ((this.xPuck - this.fieldXvsY.strickerPlayer2.xPlayer2) < 0 && (this.yPuck - this.fieldXvsY.strickerPlayer2.yPlayer2) > 0) )
                    {
                        this.vSwap = this.vxPuck;
                        this.vxPuck = this.vyPuck;
                        this.vyPuck = this.vSwap;
                    }
                    else
                    {
                        this.vSwap = (-1) * this.vxPuck;
                        this.vxPuck = (-1) * this.vyPuck;
                        this.vyPuck = this.vSwap;
                    }
                }
            }
            //si le vercteur est en diagonal
            else
            {
                if (Math.abs(this.yPuck - this.fieldXvsY.strickerPlayer2.yPlayer2) / Math.abs(this.xPuck - this.fieldXvsY.strickerPlayer2.xPlayer2) < 1)
                {
                    resortirLeJeton();
                    this.vxPuck = (-1) * this.vxPuck;
                }
                else if (Math.abs(this.yPuck - this.fieldXvsY.strickerPlayer2.yPlayer2) / Math.abs(this.xPuck - this.fieldXvsY.strickerPlayer2.xPlayer2) > 1)
                {
                    this.vyPuck = (-1) * this.vyPuck;
                }
                else
                {
                    this.vxPuck = (-1) * this.vxPuck;
                    this.vyPuck = (-1) * this.vyPuck;
                }
            }
            this.turnJeton = true;
        }

        //si le jeton a touche le mur ou un palet donc on le tournce apres avoir calcule ces vitesse avant
        if (this.turnJeton)
        {
            this.xPuck += 2*(this.vxPuck * this.tempsV / FieldGameXvsY.nbSecondeVitesse);
            this.yPuck += 2*(this.vyPuck * this.tempsV / FieldGameXvsY.nbSecondeVitesse);
            //deplacer le jeton avec les x et y calcules
            this.layout((int) (this.xPuck - rayonPuck), (int) (this.yPuck - rayonPuck), (int) (this.xPuck + rayonPuck), (int) (this.yPuck + rayonPuck));
            this.turnJeton = false;
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    //methode pour sortir de la palet en cas de collision pour ne pas avoir de bug "a cause du float et des pixels"
    protected void sortirDeLaPalet(float x, float y, float r, float vx, float vy) {
        //le rayon=distance entre centrePuck et centrePalet
//        this.r1 = (float) Math.sqrt(Math.pow((x - this.xPuck), 2) + Math.pow((y - this.yPuck), 2));
//        //le rayon=distance de superposition entre le jeton et la palet en cas de collision
//        this.r2 = (float) ((this.rayonPuck + r) - Math.sqrt(Math.pow((x - this.xPuck), 2) + Math.pow((y - this.yPuck), 2)));
//        //utiliser les cosinus et sinus et les lignes parallels pour calculer le x et y de difference plus la direction
//        if (vx > 0) {
//            this.xPuck += (Math.abs(x - this.xPuck) * this.r2) / this.r1 + 10;
//        } else if (vx < 0) {
//            this.xPuck -= (Math.abs(x - this.xPuck) * this.r2) / this.r1 + 10;
//        }
//        if (vy > 0) {
//            this.yPuck += (Math.abs(y - this.yPuck) * this.r2) / this.r1 + 10;
//        } else if (vy < 0) {
//            this.yPuck -= (Math.abs(y - this.yPuck) * this.r2) / this.r1 + 10;
//        }

        this.xPuck += vx * this.tempsV / FieldGameXvsY.nbSecondeVitesse;
        this.yPuck += vy * this.tempsV / FieldGameXvsY.nbSecondeVitesse;

        this.layout((int) (this.xPuck - rayonPuck), (int) (this.yPuck - rayonPuck), (int) (this.xPuck + rayonPuck), (int) (this.yPuck + rayonPuck));
    }
    //==========================================================================================================
    //methode pour resortir du palet si c notre jeton qui rentre dans le palet
    protected void resortirLeJeton()
    {

           this.xPuck -= this.vxPuck * this.tempsV / FieldGameXvsY.nbSecondeVitesse;
           this.yPuck -= this.vyPuck * this.tempsV / FieldGameXvsY.nbSecondeVitesse;

        //sortir le jeton avec son rayon mais avec la direction de son vecteur de vitesse
//        this.xPuck -= this.vxPuck / Math.abs(this.vxPuck) * this.rayonPuck;
//        this.yPuck -= this.vyPuck / Math.abs(this.vyPuck) * this.rayonPuck;

        //deplacer le jeton avec les x et y calcules
        this.layout((int) (this.xPuck - rayonPuck), (int) (this.yPuck - rayonPuck), (int) (this.xPuck + rayonPuck), (int) (this.yPuck + rayonPuck));
    }

    //--------------------------------------------------------------------------------------------------------------------------------
    //gerer le deplacement et collision du puke
    @Override
    public void run() {
        if (this.vxPuck != 0 || this.vxPuck != 0) {

            //deplacer le jeton selon l'equation de droite
            this.deplacerPuckByVelocity();
        }
        else
        {
            this.isRunning = false;
        }

        /**deminuer la vitesse a cause du frotement
         * avec de la physique
         */
//        if (this.vxPuck != 0)
//        {
//            this.vxPuck -= 30;
//        }
//        if (this.vyPuck != 0)
//        {
//            this.vyPuck -= 30;
//        }
//        else
//        {
//            this.xPuck = 0;
//            this.yPuck = 0;
//        }

        //gerer la vitesse du jeton en utilisant le temps d'attente
        this.handler.postDelayed(this, this.tempsV);
    }
}
