package com.example.tp_android;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ListView;

import java.util.ArrayList;

import entities.Score;
import managers.ScoreAdaptater;
import managers.ScoreManager;


public class ScoreActivity extends Activity {

    ListView lv_score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        this.lv_score = findViewById(R.id.lv_score);

        ArrayList<Score> listeToSort = ScoreManager.getAllScores(this);
        ArrayList<Score> listeScoreSorted = new ArrayList<>();
        if (listeToSort.size() > 0) {
            listeScoreSorted.add(listeToSort.get(0));
            listeToSort.remove(0);
            for (Score s : listeToSort) {
                for (int i = 0; i < listeScoreSorted.size(); i++) {
                    if (s.getScorePlayer() > listeScoreSorted.get(i).getScorePlayer()) {
                        listeScoreSorted.add(i, s);
                        i = listeScoreSorted.size()+1;
                    }
                    else if (s.getScorePlayer() < listeScoreSorted.get(i).getScorePlayer() && i == listeScoreSorted.size() - 1){
                        listeScoreSorted.add(s);
                        i = listeScoreSorted.size()+1;
                    }
                    else if (s.getScorePlayer() == listeScoreSorted.get(i).getScorePlayer())
                    {
                        listeScoreSorted.add(i+1, s);
                        i = listeScoreSorted.size();
                    }
                }
            }
        }

        ScoreAdaptater sAdp = new ScoreAdaptater(this, R.layout.score_layout , listeScoreSorted);

        this.lv_score.setAdapter(sAdp);
    }
}
