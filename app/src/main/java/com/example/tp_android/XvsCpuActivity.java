package com.example.tp_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;


public class XvsCpuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xvs_cpu);
    }


    //----------------------------------------------------------------------------------------------------------------------
    /**
     * si on clique sur Jouer on ouvre l'activity
     * du jeu contre adversaire
     * seulement si le player a deja saisie son nom
     */
    public void onClickXvsCpu(View v)
    {
        if (!((EditText)findViewById(R.id.editTextName1)).getText().toString().isEmpty())
        {
            Intent intent = new Intent(this, PartieXvsCpuActivity.class);

            intent.putExtra("name1",((EditText)findViewById(R.id.editTextName1)).getText());

            startActivityForResult(intent, 2);
        }
    }
}
