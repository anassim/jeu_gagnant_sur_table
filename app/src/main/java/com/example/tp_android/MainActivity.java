package com.example.tp_android;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.WindowManager;

import managers.ScoreManager;
import services.ConnexionBd;


public class MainActivity extends Activity {

    public static MediaPlayer gameMusic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        gameMusic = MediaPlayer.create(this, R.raw.bensound_moose);
        gameMusic.start();

        //initialiser la base de donnees
        ConnexionBd.copyBdFromAssets(this);
    }

    public void onClickVsY(View v)
    {
        Intent intent = new Intent(this, XvsYActivity.class);

        startActivityForResult(intent, 20);
    }

    public void onClickVsCpu(View v)
    {
        Intent intent = new Intent(this, XvsCpuActivity.class);

        startActivityForResult(intent, 30);
    }

    public void onClickScore(View v)
    {
        Intent intent = new Intent(this, ScoreActivity.class);

        startActivityForResult(intent, 40);
    }
}
