package com.example.tp_android;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;


public class XvsYActivity extends Activity {

    public static XvsYActivity thisActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xvs_y);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (!MainActivity.gameMusic.isPlaying())
        {
            MainActivity.gameMusic.reset();
            MainActivity.gameMusic = MediaPlayer.create(this, R.raw.bensound_moose);
            MainActivity.gameMusic.start();
        }
    }
    //----------------------------------------------------------------------------------------------------------------------
    /**
     * si on clique sur Jouer on ouvre l'activity
     * du jeu contre adversaire
     * seulement si le player a deja saisie son nom
     */
    public void onClickXvsY(View v)
    {
       if (!((EditText)findViewById(R.id.editTextName1)).getText().toString().isEmpty() && !((EditText)findViewById(R.id.editTextName2)).getText().toString().isEmpty())
       {
           thisActivity = this;

           Intent intent = new Intent(this, PartieXvsYActivity.class);

           intent.putExtra("name1",((EditText)findViewById(R.id.editTextName1)).getText().toString());
           intent.putExtra("name2",((EditText)findViewById(R.id.editTextName2)).getText().toString());

           startActivityForResult(intent, 1);

           MainActivity.gameMusic.stop();
       }
    }
}
