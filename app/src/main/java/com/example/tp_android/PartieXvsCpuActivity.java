package com.example.tp_android;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.LinearLayout;

import views.FieldGameXvsCpu;
import views.FieldGameXvsY;


public class PartieXvsCpuActivity extends Activity {

    Handler handler;
    String name1;
    LinearLayout layoutGeneral;
    FieldGameXvsCpu field;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partie_xvs_cpu);

        this.handler = new Handler();

        this.name1 = getIntent().getStringExtra("name1");

        this.layoutGeneral = findViewById(R.id.layoutGeneral);

        this.field = new FieldGameXvsCpu(this, this.handler);

        this.layoutGeneral.addView(this.field);
    }
}
