package com.example.tp_android;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.LinearLayout;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.URI;

import views.FieldGameXvsY;

import static java.lang.System.in;


public class PartieXvsYActivity extends Activity {

    Handler handler;
    public String name1 = "Nassim";
    public String name2 = "Ahmed";
    LinearLayout layoutGeneral;
    FieldGameXvsY field;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partie_xvs_y);
        this.handler = new Handler();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.name1 = getIntent().getStringExtra("name1");
        this.name2 = getIntent().getStringExtra("name2");
        this.layoutGeneral = findViewById(R.id.layoutGeneral);
        this.field = new FieldGameXvsY(this, this.handler, this, XvsYActivity.thisActivity);
        this.layoutGeneral.addView(this.field);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 3 && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            this.field.imageView.setImageBitmap(imageBitmap);
            try {
                File f = new File(field.scoreWinner.getImagePlayer());
                ByteArrayOutputStream bos = new ByteArrayOutputStream((int) f.length());
                imageBitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
                byte[] bArr = bos.toByteArray();
                bos.flush();
                bos.close();

                FileOutputStream fout = openFileOutput(field.scoreWinner.getImagePlayer(), Context.MODE_PRIVATE);
                fout.write(bArr);
                fout.flush();
                fout.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
