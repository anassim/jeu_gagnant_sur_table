package services;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.view.Display;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.Buffer;


public class ConnexionBd {

        private static SQLiteDatabase bd;
        private static String dataBase = "bd_gagnant_sur_table";
        private static int version = 1;
        public static SQLiteDatabase getBd(Context ctx){
            BdHelper hBD = new BdHelper(ctx,dataBase,null,version);
            return bd = hBD.getWritableDatabase();
        }
        public static void closeBD(){
            bd.close();
        }
        public static void copyBdFromAssets(Context ctx)
        {
            File fichierBd = ctx.getDatabasePath(dataBase);
            if (fichierBd.exists())
            {
              return;
            }

            try {
                InputStream in = ctx.getAssets().open("bd/bd_ganant_sur_table.db");
                FileOutputStream out = new FileOutputStream(fichierBd);

                byte[] buffer = new byte[256];
                while (in.read(buffer) != -1)
                {
                    out.write(buffer);
                    buffer = new byte[256];
                }

                in.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

